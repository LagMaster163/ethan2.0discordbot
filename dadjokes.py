# Import All the Things

from urllib.request import urlopen
import pprint
import discord
import asyncio
from random import randint
from random import choice
from datetime import datetime
from git import Repo
from os import getcwd
from os import system

# Constants

DEBUGGING = True
BOTISON = True
RUNNINGGIT = True

def debugging(dbstr):
	if DEBUGGING:
		print(str(datetime.today()) + " DEBUGGING: " + dbstr)

# Class for html parsing
# Code will continue if it cant decode the html in a utf-8 format, but will warn the user with print statements

class htmlp:

	def __init__(self, hstr):

		try:
			self.hstr = hstr.decode("utf-8")
		except UnicodeDecodeError as e:
			self.hstr = str(hstr)
			print(e)

	def __str__(self):
		return str(self.hstr)

	# Method that returns a list of all strings between a begin string and end string
	# doesntbeginwith is an optional parameter that excludes certain  lists that begin with a certian substring
	# eg. all strings between "foo" and "bar" that dont begin with the substring "n"

	def between(self, substring1, substring2, doesntbeginwith=''):

		retval = []

		s = self.hstr.split(substring1)

		for i in s:
			try:
				if i[0] != doesntbeginwith:
					retval.append(i[0:i.index(substring2)])

			except:
				pass

		if len(retval) == 1:
			return retval[0]
		return retval

def redditrm():
	rurl = "https://www.reddit.com/r/dankmemes/"

	rhtml = htmlp(urlopen(rurl).read())

	rtbegstr = ';utm_source=reddit&amp;utm_name=dankmemes" rel="" >'
	rtendstr = '</a>'
	rtdbw = '<'

	rpbegstr = 'utm_name=dankmemes" data-href-url="'
	rpendstr = '" data-event-action'

	titles = rhtml.between(rtbegstr, rtendstr, doesntbeginwith=rtdbw)
	pages = rhtml.between(rpbegstr, rpendstr, doesntbeginwith=rtdbw)

	randc = randint(0, min(len(pages), len(titles)))

	ctitle = titles[randc]
	cpage = "https://www.reddit.com" + pages[randc]

	ribegstr = 'class="may-blank"><img class="preview" src="'
	riendstr = '"'
	ridbw = '<'

	rpage = htmlp(urlopen(cpage).read())
	meme = rpage.between(ribegstr, riendstr)[1]
	meme = meme.replace("amp;", '')

	return ctitle, meme

def ifunnyrm(spice=0):

	xurl = 'https://ifunny.co/feeds/shuffle'
	if spice < 69:
		xurl = 'https://ifunny.co'
	xbegsrt = 'src="https://img.ifcdn.com/images/'
	xendstr = '" alt'
	xhtml = htmlp(urlopen(xurl).read())
	urltags = xhtml.between(xbegsrt, xendstr)
	return "https://img.ifcdn.com/images/" + choice(urltags)


# Website 1 parsing, pun.me

url1 = 'http://pun.me/pages/dad-jokes.php'
begstr1 = '<li>'
endstr1 = '</li>'
doesntbegw1 = "<"

html1 = htmlp(urlopen(url1).read())
lst1 = html1.between(begstr1, endstr1, doesntbeginwith=doesntbegw1)

# Website 2 parsing, buzfeed.com

url2 = "https://www.buzzfeed.com/mikespohr/75-dad-jokes-that-are-so-bad-theyre-actually-good?utm_term=.yl69wO03db#.hx9V21w6Yx"
begstr2 = '<span class="js-subbuzz__title-text">'
endstr2 = '</span>'
doesntbegw2 = '<'

html2 = htmlp(urlopen(url2).read())
lst2 = html2.between(begstr2, endstr2, doesntbeginwith=doesntbegw2)

# joining two lists of jokes

lst = lst1 + lst2


# Discord Bot

client = discord.Client()

# When the bot it ready

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    print("Dad is now active...")

# When a message is sent
# Handling different versions of I'm vs Im

@client.event
async def on_message(message):
	if message.content.startswith("I'm ") or message.content.startswith("i'm "):
		mescont = message.content[4:]
		try:
			await client.send_message(message.channel, "Hi, '" + mescont + "', I'm Dad.")
			debugging("send response to I'm on channel " + str(message.channel))
		except:
			pass

	elif message.content.startswith("Im ") or message.content.startswith("im "):
		mescont = message.content[3:]
		try:
			await client.send_message(message.channel, "Hi, '" + mescont + "', I'm Dad.")
			debugging("send response to Im on channel " + str(message.channel))
		except:
			pass

	elif message.content.lower() == ("hi dad"):
		mescont = message.content[4:]
		try:
			await client.send_message(message.channel, lst[randint(0, len(lst))])
			debugging("sent rand joke on channel " + str(message.channel))
		except:
			pass

	elif message.content.lower() == ("normie meme"):
		try:
			await client.send_message(message.channel, ifunnyrm(spice=70))
			debugging("meme on channel: " + str(message.channel))
		except:
			pass

	elif message.content.lower() == ("dank meme"):
		try:
			title, meme = redditrm()
			#await client.send_message(message.channel, title)
			await client.send_message(message.channel, meme)
			debugging("meme on channel: " + str(message.channel))
		except:
			pass

# Background Task that automatically tries to update and restart the bot

async def gitupdatechecker():
	await client.wait_until_ready()
	repo = Repo(getcwd())
	Git = repo.git()
	while not client.is_closed:
		await asyncio.sleep(60)		
		if Git.pull() != "Already up-to-date.":
			print("checked for updates, new version found, spawning new prosess...")
			print("")
			system("python3 dadjokes.py")
		else:
			debugging("checked for updates, none found")

# Running the Bot
if BOTISON:
	if RUNNINGGIT:
		client.loop.create_task(gitupdatechecker())
	client.run("MzgwODMzMjczMTEyMjk3NDc0.DPBx8Q.HTwPhj8Pqe9VJSKGDByM-VKrjnI")